<?php

/* @var $this yii\web\View */
/* @var $model \backend\models\forms\TeacherForm */

$this->title = 'Mutaxassis Qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
    'model' => $model,
]);