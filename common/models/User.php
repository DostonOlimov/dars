<?php

namespace common\models;

use common\models\defaults\DefaultActiveRecord;
use common\modules\sms\smses\EskizSms;
use common\services\TelegramService;
use console\socket\ClientSocket;
use console\socket\manager\Connector;
use cranky4\changeLogBehavior\ChangeLogBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;
use yii\web\ServerErrorHttpException;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string|null $father_name
 * @property int $gender
 * @property string $phone
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property int $temp
 * @property string $photo
 * @property int $type
 * @property integer $status
 * @property integer $orders_count
 * @property string $language
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property string $typeName
 * @property EmployeeBranch[] $branches
 */
class User extends DefaultActiveRecord implements IdentityInterface
{
    public const STATUS_DELETED = 1;
    public const STATUS_UNCONFIRMED = 5;
    public const STATUS_UNFILLED = 7;
    public const STATUS_INACTIVE = 9;
    public const STATUS_ACTIVE = 10;

    public const TYPE_ADMIN = 10;
    public const TYPE_MANAGER = 3;
    public const TYPE_ADMINSTRATOR = 6;
    public const TYPE_CONTACT = 25;
    public const TYPE_TEACHER = 50;

    //<editor-fold desc="Main">

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            
            TimestampBehavior::class,
            ChangeLogBehavior::class
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'first_name', 'last_name', 'phone', 'auth_key', 'password_hash', 'type'], 'required'],
            [['username', 'first_name', 'last_name', 'father_name', 'phone', 'auth_key', 'password_hash', 'temp'], 'string', 'max' => 255],
            [['type', 'status'], 'integer'],
            [['phone'], 'unique', 'targetAttribute' =>'type'],
            [['language'], 'string', 'max' => 5],
            [['language'], 'in', 'range' => ['uz', 'ru']],
            ['type', 'in', 'range' => array_keys(self::getTypeList())],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED, self::STATUS_UNCONFIRMED, self::STATUS_UNFILLED]],
        ];
    }

    public static function getTypeList(): array
    {
        return [
            self::TYPE_ADMIN => 'Administrator',
            self::TYPE_MANAGER => 'Boshqaruvchi',
            self::TYPE_CONTACT => 'Mijoz',
            self::TYPE_TEACHER => 'O\'qituvchi',
            self::TYPE_ADMINSTRATOR => 'Adminstrator',
        ];
    }

    public function getTypeName(): string
    {
        return self::getTypeList()[$this->type];
    }

    public function getFullName(): string
    {
        return $this->first_name . ' ' . $this->last_name . ' ' . $this->father_name;
    }

    public function getPhotoUrl(): string
    {
        if ($this->photo) {
            if($this->type == self::TYPE_CONTACT){
                return '/uploads/contact/' . $this->photo;
            }
            elseif($this->type == self::TYPE_TEACHER){
                return '/uploads/teacher-data/' . $this->photo;
            }
            return '/uploads/' . $this->photo;
        }
        return '/admin/images/defaultAvatar.png';
    }
   
    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null): ?IdentityInterface
    {      
        return static::findOne([
            'auth_key' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername(string $username): ?User
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey(): ?string
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey): ?bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword(string $password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function setPassword(string $password): void
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
 /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     * @throws \yii\base\Exception
     */
    public function generateContactPassword(): void
    {
        $this->password = Yii::$app->getSecurity()->generateRandomString();
       // $this->password = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * Generates "remember me" authentication key
     * @throws \yii\base\Exception
     */
    public function generateAuthKey(): void
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates random username
     * @throws \yii\base\Exception
     */
    public function generateUsername(): void
    {
        $this->username = Yii::$app->security->generateRandomString();
    }

    /**
     * @throws \Exception
     */
    public function generateConfirmationCode(): void
    {
    //    $this->temp = 1111;
        $this->temp = random_int(1000, 9999);
    }

    //</editor-fold>

    public static function getStatusList(): array
    {
        return [
            self::STATUS_ACTIVE => 'Faol',
            self::STATUS_INACTIVE => 'Faol emas',
            self::STATUS_DELETED => 'O\'chirilgan',
            self::STATUS_UNFILLED => "To'liq emas",
        ];
    }

    public function getStatusName(): string
    {
        return self::getStatusList()[$this->status] ?? '';
    }

   


}
