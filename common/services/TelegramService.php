<?php
/**
 * @author Olimjon G'ofurov <gofuroov@gmail.com>
 * Date: 21/01/22
 * Time: 15:14
 */

namespace common\services;

class TelegramService
{
    private static ?TelegramService $instance = null;
    private string $baseUrl = 'https://api.telegram.org/bot';

    public string $_token = "2047536560:AAHuM-5JutX9s9ZcRNOR_5melKBod_536Ow";
    public string $token = "673914101:AAGpZufNMuiKdzYPKoZKfB_ZpyoE3DGRuSs";
    public string $_chat_id = "-1001645291745";
    public string $chat_id = "560005006";

    public static function getInstance(): TelegramService
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function logArray($msg = '', $data = []){
        return 0;
        $txt = "🗓 Sana: " . date('Y-m-d H:i:s') . PHP_EOL . PHP_EOL . "ℹ️" . $msg . PHP_EOL . json_encode($data);
        $self = self::getInstance();
        $self->request([
            'chat_id' => $self->chat_id,
            'text' => $txt
        ]);
    }

    public static function log($msg = ''): void
    {
        return ;
        $msg = "🗓 Sana: " . date('Y-m-d H:i:s') . PHP_EOL . PHP_EOL . "ℹ️" . $msg;

        $self = self::getInstance();
        $self->request([
            'chat_id' => $self->chat_id,
            'text' => $msg
        ]);
    }

    public static function debug($msg = ''): void
    {
        return;
        $msg = "🗓 Sana: " . date('Y-m-d H:i:s') . PHP_EOL . PHP_EOL . "ℹ️ " . $msg;

        if (YII_DEBUG) {
            $self = self::getInstance();
            $self->request([
                'chat_id' => $self->chat_id,
                'text' => $msg
            ]);
        }
    }

    private function request(array $data = []): void
    {
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => $this->baseUrl . $this->token . '/sendMessage',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 50,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => http_build_query($data),
        ]);
        curl_exec($curl);
    }
}