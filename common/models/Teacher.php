<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * @inheritDoc
 * @property-read TeacherData $teacherData
 */
class Teacher extends User
{


    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['type'], 'default', 'value' => self::TYPE_TEACHER],
            [['type'], 'in', 'range' => [self::TYPE_TEACHER]],
        ]);
    }


    public static function find()
    {
        return parent::find()->andWhere(['user.type' => self::TYPE_TEACHER]);
    }

    public function getTeacherData(): ActiveQuery
    {
        return $this->hasOne(TeacherData::class, ['teacher_id' => 'id']);
    }
}