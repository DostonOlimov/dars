<?php

use yii\helpers\Html;
use yii\bootstrap5\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var common\models\Rooms $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="card">
    <div class="card-body">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-md-6 col-lg-4 ">
    <?= $form->field($model, 'room_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6 col-lg-4 ">
    <?= $form->field($model, "branch_id")->dropDownList(ArrayHelper::map($branches, 'id', 'name'),['prompt'=>'- - -']) ?>
    </div>
    <div class="col-md-6 col-lg-4 ">
    <?= $form->field($model, 'capacity')->textInput() ?>
    </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
