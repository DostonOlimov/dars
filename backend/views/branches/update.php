<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Branches $model */

$this->title = 'O\'zgartirish: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Filialar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'O\'zgartirish';
?>
<div class="branches-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'states'=>$states
    ]) ?>

</div>
