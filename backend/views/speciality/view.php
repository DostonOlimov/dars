<?php

use common\models\Speciality;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Speciality */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['update'] = Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
$this->params['delete'] = $model->status === Speciality::STATUS_ACTIVE ? Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-warning',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni o\'chirmoqchimisiz?',
        'method' => 'post',
    ],
]) : '';

$this->params['other'] = $model->status === Speciality::STATUS_DELETED ? Html::a('<i class="fa fa-trash"></i>', ['force-delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni o\'chirmoqchimisiz?',
        'method' => 'post',
    ],
]) : '';
?>

<div class="card">
    <div class="card-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                [
                    'attribute' => 'status',
                    'value' => static function (\common\models\Speciality $speciality) {
                        return $speciality->getStatusName();
                    }
                ],
            ],
        ]) ?>
    </div>
</div>