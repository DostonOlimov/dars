<?php

namespace common\models\searchs;

use common\models\Employee;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * EmployeeSearch represents the model behind the search form of `common\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'first_name', 'last_name', 'father_name', 'phone', 'auth_key', 'password_hash', 'password_reset_token', 'temp', 'photo', 'language'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, ActiveQuery $query = null)
    {
        if (!$query) {
            $query = Employee::find();
        }

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions
        $query->andFilterWhere([
            'employee.id' => $this->id,
            'employee.type' => $this->type,
            'employee.status' => $this->status,
            'employee.created_at' => $this->created_at,
            'employee.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'employee.username', $this->username])
            ->andFilterWhere(['like', 'employee.first_name', $this->first_name])
            ->andFilterWhere(['like', 'employee.last_name', $this->last_name])
            ->andFilterWhere(['like', 'employee.father_name', $this->father_name])
            ->andFilterWhere(['like', 'employee.phone', $this->phone])
            ->andFilterWhere(['like', 'employee.auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'employee.password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'employee.password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'employee.temp', $this->temp])
            ->andFilterWhere(['like', 'employee.photo', $this->photo])
            ->andFilterWhere(['like', 'employee.language', $this->language]);

        return $dataProvider;
    }
}
