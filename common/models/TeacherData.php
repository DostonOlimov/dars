<?php

namespace common\models;

use common\services\FilterService;
use yii\db\ActiveQuery;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "teacher_data".
 *
 * @property int $id
 * @property int $teacher_id
 * @property string|null $born
 * @property string|null $series
 * @property string|null $number
 * @property string|null $passport_sn
 * @property string|null $given_by
 * @property $given_date
 * @property string|null $photo_url
 * @property int $status
 * @property string $address
 * @property string $pics
 * @property int $speciality_id
 * @property int $freelancer
 * @property int $gender
 * @property int $branch_id
 * @property int $model_id
 * @property int $color_id
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property User $teacher
 * @property Speciality $speciality
 * @property Branch $branch
 * @property DirectoryColors $carColor
 * @property DirectoryCars $carModel
 */
class TeacherData extends \common\models\defaults\DefaultActiveRecord
{
    /* */
    public const GENDER_MAN = 6;
    public const GENDER_WOMAN = 8;

    public ?UploadedFile $file = null;
    public $photo;
    public $temp = null;
    public $pics = null;
    public string $workingDir = "@uploads/teacher-data/";



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id', 'born', 'speciality_id', 'gender', 'branch_id'], 'required'],
            [['teacher_id'], 'unique', 'message' => "Ushbu mutaxassis ma'lumotlari avval kiritilgan."],
            // [['photo'], 'required', 'when' => function () {
            //     return $this->isNewRecord;
            // }, 'whenClient' => "function (attribute, value) {
            //     return $this->isNewRecord;
            // }"],
            [['photo'], 'file', 'extensions' => 'jpg, png, jpeg', 'maxSize' => 1024 * 1024 * 10],
            [['teacher_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by', 'speciality_id', 'gender', 'branch_id'], 'integer'],
            [['address'], 'string'],
            [['born'], 'date', 'format' => 'php:Y-m-d'],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['photo_url'], 'string', 'max' => 255],
            [['photo'], 'safe'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['teacher_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['speciality_id'], 'exist', 'targetClass' => Speciality::class, 'targetAttribute' => ['speciality_id' => 'id']],
            [['gender'], 'in', 'range' => array_keys(self::getGenderList())],
            [['branch_id'], 'exist', 'targetClass' => Branches::class, 'targetAttribute' => ['branch_id' => 'id']],
        ];
    }

    /**
     * Gets query for [[teacher]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::class, ['id' => 'teacher_id']);
    }
    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    public function upload(): bool
    {
        $this->file = $this->file ?? UploadedFile::getInstance($this, 'photo');
        if ($this->file) {
            $this->photo = '1';
        }
        if (!$this->validate()) {
            return false;
        }
        if (!$this->photo) {
            $this->photo = '';
            return true;
        }

        $name = \Yii::$app->security->generateRandomString(16) . "." . $this->file->extension;
        if (FileHelper::createDirectory(\Yii::getAlias($this->workingDir)) && $this->file->saveAs($this->workingDir . $name)) {
            //Delete old file if exists
            if ($this->photo_url) {
                $oldFilePath = \Yii::getAlias($this->workingDir) . $this->photo_url;
                if (file_exists($oldFilePath)) {
                    unlink($oldFilePath);
                }
            }
            $this->photo_url = $name;
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        if ($this->photo_url) {
            return '/uploads/teacher-data/' . $this->photo_url;
        }
        return '';
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->photo_url) {
            $oldFilePath = \Yii::getAlias($this->workingDir) . $this->photo_url;
            if (file_exists($oldFilePath)) {
                unlink($oldFilePath);
            }
        }
        return parent::beforeDelete();
    }

    /**
     * @return string[]
     */
    public static function getGenderList(): array
    {
        return [
            self::GENDER_MAN => 'Erkak',
            self::GENDER_WOMAN => 'Ayol',
        ];
    }

    /**
     * @return string
     */
    public function getGenderName(): string
    {
        return self::getGenderList()[$this->gender] ?? '';
    }

    /**
     * @return ActiveQuery
     */
    public function getSpeciality(): ActiveQuery
    {
        return $this->hasOne(Speciality::class, ['id' => 'speciality_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getBranch(): ActiveQuery
    {
        return $this->hasOne(Branches::class, ['id' => 'branch_id']);
    }

    public static function find()
    {
        $find = parent::find();

        if (FilterService::check()) {
            /** @var ?User $user */
            $user = FilterService::getUser();
            $find
                ->andWhere([
                    'in',
                    'teacher_data.branch_id',
                    $user->getBranches()->select('branch_id')->asArray()->cache(10)->column()
                ]);
        }

        return $find;
    }
}
