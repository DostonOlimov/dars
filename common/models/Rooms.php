<?php

namespace common\models;

use Yii;
use common\models\defaults\DefaultActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rooms".
 *
 * @property int $id
 * @property string $room_name
 * @property int $branch_id
 * @property int $capacity
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Branches $branch
 */
class Rooms extends DefaultActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_name', 'branch_id', 'capacity'], 'required'],
            [['branch_id', 'capacity'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['room_name'], 'string', 'max' => 255],
            [['branch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Branches::class, 'targetAttribute' => ['branch_id' => 'id']],
        ];
    }

    /**
     * Gets query for [[Branch]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branches::class, ['id' => 'branch_id']);
    }
}
