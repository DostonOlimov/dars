<?php

use common\models\Branches;
use common\models\Cities;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var common\models\searchs\BranchesSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Filiallar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branches-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Qo\'shish', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'attribute' => 'state_id',
                'label' => 'Hudud',
                'value' => function (Branches $model) {
                    return $model->city_id ? $model->city->state->name : '';
                },
                'filter' => Html::activeDropDownList($searchModel, 'state_id', ArrayHelper::map($states, 'id', 'name'), ['class' => 'form-control', 'prompt' => '- - -'])
            ],
            [
                'attribute' => 'city_id',
                'label' => 'Shahar yoki tuman',
                'value' => function (Branches $model) {
                    return $model->city_id ? $model->city->name : '';
                },
              //  'filter' => Html::activeDropDownList($searchModel, 'city_id', ArrayHelper::map($states, 'id', 'name'), ['class' => 'form-control', 'prompt' => '- - -'])
            ],
            'address:ntext',
        
            'foundated_at',
            //'created_at',
            //'updated_at',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Branches $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
