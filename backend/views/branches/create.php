<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Branches $model */

$this->title = 'Fillial qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Filliallar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="branches-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'states'=> $states
    ]) ?>

</div>
