<?php

use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\searchs\NurseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mutaxassislar (Arxiv)';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Arxiv
                    </div>
                    <div class="card-tools">
                        <?= \yii\bootstrap4\Html::a('Faollar', ['nurse/index'], [
                            'data-pjax' => 0
                        ]) ?>
                    </div>
                </div>
                <div class="card-body">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => SerialColumn::class],

                            [
                                'attribute' => 'first_name',
                                'value' => static function (\common\models\Teacher $nurse) {
                                    return $nurse->getFullName();
                                }
                            ],
                            [
                                'class' => \common\columns\StatusColumn::class
                            ],
                            [
                                'class' => ActionColumn::class,
                                'template' => '{view} {force-delete}',
                                'buttons' => [
                                    'force-delete' => static function ($url, $model, $key) {
                                        return Html::a('<i class="fa fa-trash text-danger"></i>', $url, [
                                            'data' => [
                                                'pjax' => 0,
                                                'confirm' => "Siz rostdan ham to'liq ochirmoqchimisiz?"
                                            ]
                                        ]);
                                    }
                                ]
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>