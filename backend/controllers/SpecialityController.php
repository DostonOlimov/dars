<?php

namespace backend\controllers;

use common\models\searchs\SpecialitySearch;
use common\models\Branches;
use common\models\Speciality;
use yii\web\NotFoundHttpException;

/**
 * SpecialityController implements the CRUD actions for Speciality model.
 */
class SpecialityController extends BaseController
{

    /**
     * Lists all Speciality models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SpecialitySearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Speciality::find()->andWhere(['!=', 'status', Speciality::STATUS_DELETED]));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionArchive(): string
    {
        $searchModel = new SpecialitySearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Speciality::find()->andWhere(['=', 'status', Speciality::STATUS_DELETED]));

        return $this->render('archive', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionChange($id)
    {
        $model = $this->findModel($id);
        $model->status = Branches::STATUS_ACTIVE;
        $model->save();
        return $this->redirect(\Yii::$app->request->referrer);
    }

    /**
     * Displays a single Speciality model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Speciality model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Speciality();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Speciality model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Speciality model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionForceDelete(int $id)
    {
        $this->findModel($id)->delete(true);

        return $this->redirect(['archive']);
    }

    /**
     * Finds the Speciality model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Speciality the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Speciality::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
