<?php
use yii\caching\FileCache;
use yii\i18n\Formatter;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'container' => [
        'definitions' => [
            \yii\widgets\LinkPager::class => \yii\bootstrap4\LinkPager::class,
        ],
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            ],
        ],
        'formatter' => [
            'class' => Formatter::class,
            'defaultTimeZone' => 'Asia/Tashkent',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',
            'currencyCode' => 'UZS',
            'datetimeFormat' => 'php:M d, Y\y H:i',
            'timeFormat' => 'HH:mm:ss'
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
    ],
];
