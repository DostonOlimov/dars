<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\searchs\TeacherSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'O\'qituvchilar';
$this->params['breadcrumbs'][] = $this->title;
$this->params['create'] = Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success'])
?>

<?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card collapsed-card">
                <div class="card-header">
                    <h3 class="card-title">Qidiruv</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Faollar
                    </div>
                    <div class="card-tools">
                        <?= \yii\bootstrap4\Html::a('Arxiv', ['teacher/archive'], [
                            'data-pjax' => 0
                        ]) ?>
                    </div>
                </div>
                <div class="card-body">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => SerialColumn::class],

                            [
                                'attribute' => 'first_name',
                                'value' => static function (\common\models\Teacher $teacher) {
                                    return $teacher->getFullName();
                                }
                            ],
                            [
                                'class' => \common\columns\StatusColumn::class
                            ],
                            [
                                'class' => ActionColumn::class,
                                'template' => '{view} {delete}',
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>
