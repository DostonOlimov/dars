<?php

namespace backend\models\forms;

use common\models\Teacher;
use yii\base\Model;

class TeacherPasswordForm extends Model
{
    public $password;
    public $password_repeat;

    public function rules()
    {
        return [
            [['password', 'password_repeat'], 'required'],
            [['password', 'password_repeat'], 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @param Teacher $teacher
     * @return bool
     * @throws \yii\base\Exception
     */
    public function save(Teacher $teacher): bool
    {
        if (!$this->validate()) {
            return false;
        }
        $teacher->setPassword($this->password);
        $teacher->generateAuthKey();
        return $teacher->save();
    }
}