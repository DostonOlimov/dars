<?php

namespace common\models\searchs;

use common\models\Teacher;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * TeacherSearch represents the model behind the search form of `common\models\Teacher`.
 */
class TeacherSearch extends Teacher
{
    public ?string $born = '';
    public ?string $address = null;
    public $data_status = null;
    public $speciality_id = null;
    public $freelancer = null;
    public $gender = null;
    public $branch_id = null;
    public $father_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'first_name', 'last_name', 'father_name', 'phone', 'auth_key', 'password_hash', 'password_reset_token', 'temp', 'photo', 'language'], 'safe'],
            [['born', 'address'], 'string'],
            [['data_status', 'speciality_id', 'gender'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, ActiveQuery $query = null)
    {
        if (!$query) {
            $query = Teacher::find();
        }
        $query->leftJoin('teacher_data', 'teacher_data.teacher_id = user.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user.id' => $this->id,
            'user.type' => $this->type,
            'user.status' => $this->status,
        ]);

        $query
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'father_name', $this->father_name])
            ->andFilterWhere(['like', 'phone', $this->phone]);

        $query
            ->andFilterWhere(['like', 'teacher_data.born', $this->born])
            ->andFilterWhere(['like', 'teacher_data.address', $this->address])
            ->andFilterWhere([
                'teacher_data.status' => $this->data_status,
                'teacher_data.speciality_id' => $this->speciality_id,
                'teacher_data.gender' => $this->gender,
                'teacher_data.branch_id' => $this->branch_id,
            ]);

        return $dataProvider;
    }
}
