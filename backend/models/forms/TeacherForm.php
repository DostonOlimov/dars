<?php

namespace backend\models\forms;

use common\models\Teacher;
use common\models\TeacherData;
use common\models\User;
use DateTime;
use common\models\defaults\DefaultActiveRecord;
use yii\web\UploadedFile;

class TeacherForm extends \yii\base\Model
{
    protected ?int $_id = null;
    public ?string $first_name = null;
    public ?string $last_name = null;
    public ?string $father_name = null;
    public ?string $phone = null;
    public ?string $password = null;
    public ?string $password_repeat = null;
    public ?string $born = null;
    public ?string $model_id = null;

    public ?UploadedFile $file = null;
    public $photo;
    public ?string $photoName = null;
    private string $workingDir = "@uploads/teacher-data/";

    public ?string $address = null;
    public $speciality_id = null;
    public ?int $gender = null;
    public $branch_id = null;
    public $temp = null;

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'father_name', 'phone', 'born',  'address', 'speciality_id', 'gender', 'branch_id'], 'required'],
            [['speciality_id', 'gender', 'branch_id', 'model_id'], 'integer'],
            [['phone'], 'unique', 'targetClass' => User::class, 'targetAttribute' => 'phone'],
            [['born'], 'date', 'format' => 'php:Y-m-d'],
            [[ 'photo', 'pics'], 'safe'],
            [['photo'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxSize' => 1024 * 1024 * 10, 'tooBig' => 'Файл не должен превышать 10 Мб'],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'compare', 'compareAttribute' => 'password'],
            ['password', 'default', 'value' => '12312389'],
        ];
    }

    /**
     * @throws \yii\base\Exception
     */
    public function save(): bool
    {
        $this->file = UploadedFile::getInstance($this, 'photo');
        if ($this->file) {
            $this->photo = '1';
        }
        if (!$this->validate()) {
            return false;
        }
       $teacher = new Teacher([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'father_name' => $this->father_name,
            'phone' => $this->phone,
            'type' => Teacher::TYPE_TEACHER,
        ]);
       $teacher->generateUsername();
       $teacher->setPassword($this->password);
       $teacher->generateAuthKey();
        if ($teacher->save()) {
            $this->_id =$teacher->id;
           $teacherData = new TeacherData([
                'teacher_id' =>$teacher->id,
                'born' => $this->born,
                'address' => $this->address,
                'speciality_id' => $this->speciality_id,
                'gender' => $this->gender,
                'branch_id' => $this->branch_id,
            ]);
           $teacherData->file = $this->file;
            if ($teacherData->upload()) {
                $this->temp =$teacherData->photo_url;
                if($teacherData->save()){
                    $teacher->photo = $teacherData->photo_url;
                    $teacher->save();
                    return true;
                }
                
            }
        }
        // else {
        //     var_dump($teacher->type);
        //     die();
        // }

        return false;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->_id;
    }
}