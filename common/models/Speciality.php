<?php

namespace common\models;

use common\behaviors\DefaultMultilingualBehavior;
use common\models\defaults\DefaultActiveRecord;
use cranky4\changeLogBehavior\ChangeLogBehavior;
use yii\db\ActiveQueryInterface;

/**
 * This is the model class for table "speciality".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class Speciality extends DefaultActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'speciality';
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['name'], 'string', 'max' => 255],
        ];
    }

}
