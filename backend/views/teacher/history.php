<?php

/**
 * @var $this \yii\web\View
 * @var $nurse \common\models\Nurse
 * @var $balanceProvider \yii\data\ActiveDataProvider
 * @var $paynet \backend\models\forms\PaynetForm
 */

use common\models\Balance;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;

$this->title = "Balansi";
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $nurse->getFullName(), 'url' => ['view', 'id' => $nurse->id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6">
        <h3 class="text-center">«<?= $nurse->getFullname() ?>» ning hozirgi balansi</h3>
        <p class="text-center" style="font-size: xxx-large">
            <?= Yii::$app->formatter->asCurrency($nurse->getTotalBalance()) ?>
        </p>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Pul tushurish
                </h3>
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin(); ?>

                <?= $form->field($paynet, 'money')->textInput() ?>

                <?= $form->field($paynet, 'info')->textarea([
                    'rows' => 4
                ]) ?>

                <div class="row">
                    <div class="col d-flex justify-content-end">
                        <?= Html::submitButton('Hisobni to\'ldirish', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Mutaxassis balans tarixi</h3>
            </div>
            <div class="card-body">
                <?= GridView::widget([
                    'dataProvider' => $balanceProvider,
                    'columns' => [
                        ['class' => SerialColumn::class],
                        [
                            'attribute' => 'type',
                            'value' => static function (Balance $model) {
                                return $model->getTypeName();
                            },
                            'filter' => Balance::getTypeList(),
                        ],
                        'value:currency',
                        [
                            'attribute' => 'reason',
                            'value' => static function (Balance $model) {
                                return $model->getReasonName();
                            },
                            'filter' => Balance::getReasonList(),
                        ],
                        'created_at:datetime',
                        [
                            'class' => ActionColumn::class,
                            'template' => '{view}',
                            'buttons' => [
                                'view' => static function ($url, $model, $key) {
                                    return \yii\bootstrap4\Html::a("<i class='fa fa-eye'></i>", ['balance/view', 'id' => $model->id]);
                                }
                            ]
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>

