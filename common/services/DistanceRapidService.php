<?php


namespace common\services;


class DistanceRapidService
{
    public static function calculateDistance($lat1, $lon1, $lat2, $lon2){


        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://distance-calculator.p.rapidapi.com/distance/simple?lat_1={$lat1}&long_1={$lon1}&lat_2={$lat2}&long_2={$lon2}&unit=miles&decimal_places=2",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json",
                "X-RapidAPI-Host: distance-calculator.p.rapidapi.com",
                "X-RapidAPI-Key: 0a9c7f7bc1msh4961090d015ec46p1447dejsna14097686897"
            ],
        ]);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }
}