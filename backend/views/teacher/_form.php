<?php

use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \backend\models\forms\TeacherForm */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
            'id' => 'teacher-form',
//            'enableAjaxValidation' => true,
        ]); ?>


<div class="card">
    <div class="card-body">

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'father_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'phone', [
                    'enableAjaxValidation' => true
                ])->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '+\9\98 99 999 99 99',
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'born')->widget(DatePicker::class, [
                    'options' => ['placeholder' => 'Enter birth date ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'speciality_id')->widget(\kartik\select2\Select2::class, [
                    'data' => \common\models\Speciality::getList(true),
                    'options' => [
                        'placeholder' => 'Select a state ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            
            <div class="col-md-4">
                <?= $form->field($model, 'gender')->dropdownList(\common\models\TeacherData::getGenderList()) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'branch_id')->widget(\kartik\select2\Select2::class, [
                    'data' => \common\models\Branches::getList(true),
                    'options' => [
                        'placeholder' => 'Select a state ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'password')->passwordInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'password_repeat')->passwordInput() ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'photo')->fileInput([
                    'accept' => 'image/*',
                    'enableAjaxValidation' => false
                ]) ?>
            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col d-flex justify-content-end">
        <?= Html::submitButton('Saqlash', ['class' => 'btn
        btn-success']) ?>
    </div>
</div>


<?php ActiveForm::end(); ?>