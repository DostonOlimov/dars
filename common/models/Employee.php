<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 *Employee Model
 *
 * @property-read \yii\db\ActiveQuery $employeeBranches
 * @noinspection PropertiesInspection
 */
class Employee extends User
{
    public array $branches = [];

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'father_name', 'username', 'phone', 'type', 'branches'], 'required'],
            [['first_name', 'last_name', 'father_name', 'phone'], 'string', 'max' => 255],
            [['type'], 'integer'],
            [['phone'], 'unique'],
            [['type'], 'in', 'range' => [self::TYPE_MANAGER, self::TYPE_ADMINSTRATOR]],
            [['branches'], 'each', 'rule' => ['exist', 'targetClass' => Branches::class, 'targetAttribute' => 'id']],
            ['username', 'trim'],
            ['username', 'unique']
        ];
    }

    public static function find()
    {
        return parent::find()
            ->andWhere([
                'in',
                'type',
                [
                    self::TYPE_MANAGER,
                    self::TYPE_ADMINSTRATOR,
                ],
            ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        EmployeeBranch::deleteAll(['user_id' => $this->id]);
        foreach ($this->branches as $branch) {
            $employeeBranch = new EmployeeBranch([
                'user_id' => $this->id,
                'branch_id' => $branch,
            ]);
            $employeeBranch->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        $this->branches = EmployeeBranch::find()
            ->where(['user_id' => $this->id])
            ->select('branch_id')
            ->column();
        parent::afterFind();
    }

    public function getEmployeeBranches(): ActiveQuery
    {
        return $this->hasMany(EmployeeBranch::class, ['user_id' => 'id']);
    }
}