<?php

use common\models\Teacher;
use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\searchs\TeacherSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="Teacher-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'id') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'last_name') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'father_name') ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'phone') ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'status')->dropdownList(Teacher::getStatusList(), [
                'prompt' => '--'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'born')->widget(DatePicker::class, [
                'options' => ['placeholder' => 'Enter birth date ...'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]); ?>
        </div>
       
        <div class="col-md-3">
            <?php echo $form->field($model, 'address') ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'speciality_id')->widget(\kartik\select2\Select2::class, [
                'data' => \common\models\Speciality::getList(false),
                'options' => [
                    'placeholder' => 'Select a speciality ...'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'freelancer')->dropdownList([
                '0' => "Yo'q",
                '1' => "Ha",
            ], [
                'prompt' => '--'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'gender')->dropdownList(\common\models\TeacherData::getGenderList(), [
                'prompt' => '--'
            ]) ?>
        </div>
        <div class="col-md-3">
            <?php echo $form->field($model, 'branch_id')->widget(\kartik\select2\Select2::class, [
                'data' => \common\models\Branches::getList(false),
                'options' => [
                    'placeholder' => 'Select a branch ...'
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col d-flex justify-content-end">
            <div class="form-group">
                <?= Html::submitButton('Qidirish', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Tozlash', ['class' => 'btn btn-outline-secondary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
