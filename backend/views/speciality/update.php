<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Speciality */

$this->title = 'Mutaxassislikni tahrirlash: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislik', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';

echo $this->render('_form', [
'model' => $model,
]);