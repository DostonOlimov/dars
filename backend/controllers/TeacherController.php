<?php

namespace backend\controllers;

use backend\models\forms\TeacherForm;
use backend\models\forms\TeacherPasswordForm;
use backend\models\forms\PaynetForm;
use common\models\searchs\TeacherSearch;
use common\models\Balance;
use common\models\Teacher;
use common\models\TeacherData;
use common\models\Order;
use common\models\OrderControl;
use yii\bootstrap4\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TeacherController implements the CRUD actions for Teacher model.
 */
class TeacherController extends BaseController
{
    /**
     * Lists all Teacher models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TeacherSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Teacher::find()->andWhere(['!=', 'user.status', Teacher::STATUS_DELETED]));

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionArchive(): string
    {
        $searchModel = new TeacherSearch();
        $dataProvider = $searchModel->search($this->request->queryParams, Teacher::find()->andWhere(['=', 'user.status', Teacher::STATUS_DELETED]));

        return $this->render('archive', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Teacher model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        $model = $this->findModel($id);

        // $dataProvider = new ActiveDataProvider([
        //     'query' => Order::find()
        //         ->leftJoin('order_history', 'order_history.order_id = order.id')
        //         ->andWhere([
        //             'order_history.teacher_id' => $model->id
        //         ]),
        //     'sort' => [
        //         'defaultOrder' => [
        //             'id' => SORT_DESC,
        //         ]
        //     ],
        // ]);

        // $orderControlProvider = new ActiveDataProvider([
        //     'query' => OrderControl::find()
        //         ->andWhere([
        //             'order_control.Teacher_id' => $model->id
        //         ]),
        //     'sort' => [
        //         'defaultOrder' => [
        //             'id' => SORT_DESC,
        //         ]
        //     ],
        // ]);

        return $this->render('view', [
            'model' => $model,
        //    'dataProvider' => $dataProvider,
         //   'orderControlProvider' => $orderControlProvider,
        ]);
    }

    /**
     * Creates a new Teacher model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response|array
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $model = new TeacherForm();
    
        if ($this->request->isAjax && $model->load($this->request->post())) {
            $this->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->getId()]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Teacher model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Teacher model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionForceDelete(int $id)
    {
        $this->findModel($id)->delete(true);

        return $this->redirect(['archive']);
    }

    /**
     * Finds the Teacher model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Teacher the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Teacher::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionActivate(int $id): \yii\web\Response
    {
        $model = $this->findModel($id);
        $model->status = Teacher::STATUS_ACTIVE;
        $model->save();
        return $this->redirect($this->request->referrer ?? ['index']);
    }

    public function actionUpdatePassword(int $id)
    {
        $teacher = $this->findModel($id);
        $model = new TeacherPasswordForm();

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save($teacher)) {
            return $this->redirect(['view', 'id' => $teacher->id]);
        }

        return $this->render('update-password', [
            'model' => $model,
            'teacher' => $teacher,
        ]);
    }

    /**
     * @throws \yii\base\Exception
     * @throws NotFoundHttpException
     */
    public function actionUpdateData(int $id)
    {
        $Teacher = $this->findModel($id);
        $model = TeacherData::find()->where(['Teacher_id' => $Teacher->id])->one();
        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($this->request->isPost && $model->load($this->request->post()) /*&& $model->upload()*/ ) {
            if ($model->save()) {

                return $this->redirect(['view', 'id' => $Teacher->id]);
            } else {
                print_r($model->errors);
                die('An error occurred');
            }
        }
//        print_r($this->request->post());
//        die('The requested');

        return $this->render('update-data', [
            'model' => $model,
            'Teacher' => $Teacher,
        ]);
    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionBalanceHistory(int $id)
    {
        $Teacher = $this->findModel($id);

        $paynet = new PaynetForm([
            'Teacher_id' => $Teacher->id
        ]);

        if ($this->request->isPost && $paynet->load($this->request->post()) && $paynet->pay()) {
            return $this->refresh();
        }

        $balanceProvider = new ActiveDataProvider([
            'query' => Balance::find()
                ->andWhere([
                    'balance.Teacher_id' => $id
                ]),
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
        ]);

        return $this->render('history', [
            'balanceProvider' => $balanceProvider,
            'Teacher' => $Teacher,
            'paynet' => $paynet
        ]);
    }
}
