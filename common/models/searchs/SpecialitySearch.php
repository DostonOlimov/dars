<?php

namespace common\models\searchs;

use common\models\Speciality;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * SpecialitySearch represents the model behind the search form of `common\models\Speciality`.
 */
class SpecialitySearch extends Speciality
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {

// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, ActiveQuery $query = null)
    {
        if (!$query) {
            $query = Speciality::find();
        }
   //     $query->leftJoin('speciality_lang', 'speciality_lang.owner_id = speciality.id');

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'speciality.id' => $this->id,
            'speciality.status' => $this->status,

        ]);

        $query->andFilterWhere(['like', 'speciality.name', $this->name]);

        return $dataProvider;
    }
}
