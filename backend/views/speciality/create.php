<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Speciality */

$this->title = 'Mutaxassislik Qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo $this->render('_form', [
'model' => $model,
]);