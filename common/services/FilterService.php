<?php

namespace common\services;

use common\models\User;
use Yii;

class FilterService
{
    private static ?User $user = null;


    public static function check(): bool
    {
        if (Yii::$app->hasProperty('user')) {
            $userPro = Yii::$app->user;
            if ($userPro) {
                /** @var User $user */
                self::$user = $userPro->identity;
                /** Manager va Caller ni tekshiramiz sababi ularda ko'p branch bor */
                if (self::$user && (self::$user->type === User::TYPE_MANAGER )) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function getUser(): ?User
    {
        return self::$user;
    }
}