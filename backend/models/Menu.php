<?php

namespace backend\models;

use common\widgets\AdminLteMenuWidget;
use Yii;

class Menu extends \yii\base\Model
{
    public static function getData(): string
    {
        return AdminLteMenuWidget::widget([
            'items' => [
                [
                    'label' => 'Bosh sahifa',
                    'icon' => 'home',
                    'url' => ['/site/index'],
                ],
                [
                    'label' => 'Bildirishnomalar',
                    'icon' => 'bell',
                    'url' => ['/notification/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'notification/') !== false
                ],
                [
                    'label' => 'Chat',
                    'icon' => 'comments',
                    'url' => ['/chat/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'chat/') !== false
                ],
                [
                    'label' => 'Balans',
                    'icon' => 'wallet',
                    'url' => ['/balance/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'balance/') !== false
                ],
                [
                    'label' => 'Filiallar',
                    'icon' => 'code-branch',
                    'url' => ['/branches/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'branch') !== false
                ],
                [
                    'label' => 'Mutaxassisliklar',
                    'icon' => 'graduation-cap',
                    'url' => ['/speciality/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'speciality') !== false
                ],
                [
                    'label' => 'Xonalar',
                    'icon' => 'tags',
                    'url' => ['/rooms/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'service/') !== false
                ],
                [
                    'label' => 'Ishchilar',
                    'icon' => 'user-tie',
                    'url' => ['/employee/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'employee/') !== false
                ],
                [
                    'label' => 'O\'qituvchilar',
                    'icon' => 'chalkboard-teacher',
                    'url' => ['/teacher/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'teacher/') !== false
                ],
                /*[
                    'label' => 'Mutaxassis ma\'lumotlari',
                    'icon' => 'id-card',
                    'url' => ['/nurse-data/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'nurse-data') !== false
                ],*/
                [
                    'label' => "Xarita",
                    'icon' => 'earth-asia',
                    'url' => ['/site/map'],
                ],
                // [
                //     'label' => 'Manzil guruhlari',
                //     'icon' => 'compass',
                //     'url' => ['/address-category/index'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'address-category/') !== false
                // ],
                // [
                //     'label' => 'Manzillar',
                //     'icon' => 'location-dot',
                //     'url' => ['/address/index'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'address/') !== false
                // ],
                // [
                //     'label' => 'Mijozlar',
                //     'icon' => 'users',
                //     'url' => ['/contact/index'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'contact/') !== false
                // ],
                // [
                //     'label' => 'Buyurtmalar',
                //     'icon' => 'shopping-cart',
                //     'url' => ['/order/index'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'order/') !== false
                // ],
                [
                    'label' => "Qo'shimcha",
                    'icon' => 'object-group',
                    'items' => [
                        [
                            'label' => 'Salomlashuv ovozi',
                            'icon' => 'volume-low',
                            'url' => ['/welcome-audio'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'welcome-audio') !== false,
                        ],
                        [
                            'label' => 'Slider',
                            'icon' => 'file-image',
                            'url' => ['/slider'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'slider') !== false,
                        ],
                        [
                            'label' => 'Videolar',
                            'icon' => 'video',
                            'url' => ['/video'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'video') !== false,
                        ],
                        [
                            'label' => 'Instruction',
                            'icon' => 'circle-question',
                            'url' => ['/instruction'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'instruction') !== false,
                        ],
                        [
                            'label' => 'Litsenziya',
                            'icon' => 'drivers-license',
                            'url' => ['/license'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'instruction') !== false,
                        ],
                    ]
                ],
                [
                    'label' => 'Statistika',
                    'icon' => 'chart-line',
                    'items' => [
                        [
                            'label' => 'Buyurtmalar',
                            'icon' => 'chart-column',
                            'url' => ['/statistics/index'],
                        ],
                        [
                            'label' => 'Balans',
                            'icon' => 'sack-dollar',
                            'url' => ['/statistics/balance'],
                        ],
                    ]
                ],
                // [
                //     'label' => 'Sms xizmati',
                //     'icon' => 'sms',
                //     'url' => ['/sms/default/'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'sms/') === 0,
                // ],
                
                // [
                //     'label' => 'Sozlamalar',
                //     'icon' => 'gear fa-spin',
                //     'url' => ['/settings/index'],
                //     'active' => strpos(Yii::$app->controller->getRoute(), 'settings/') === 0
                // ],

                [
                    'label' => "Sozlamalar",
                    'icon' => 'gear fa-spin',
                    'items' => [
                        [
                            'label' => 'Sozlamalar',
                            'icon' => 'gear',
                            'url' => ['/settings/index'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'settings/') === 0
                        ],
                        [
                            'label' => 'Avto Modellari',
                            'icon' => 'car',
                            'url' => ['/directory-cars/index'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'cars') !== false,
                        ],
                        [
                            'label' => 'Avto Ranglari',
                            'icon' => 'object-group',
                            'url' => ['/directory-colors/index'],
                            'active' => strpos(Yii::$app->controller->getRoute(), 'colors') !== false,
                        ],
                        
                    ]
                ],

                [
                    'label' => "Viloyatlar",
                    'icon' => 'map',
                    'url' => ['/state/index'],
                ],
                [
                    'label' => "Shahar va tumanlar",
                    'icon' => 'city',
                    'url' => ['/cities/index'],
                ],

                [
                    'label' => "",
                ],
                [
                    'label' => 'Socketlar',
                    'icon' => 'circle-radiation fa-spin',
                    'url' => ['/socket/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'socket/') !== false
                ],
                [
                    'label' => 'Location',
                    'icon' => 'location-arrow',
                    'url' => ['/location/index'],
                    'active' => strpos(Yii::$app->controller->getRoute(), 'location/') !== false
                ],
            ]
        ]);
    }
}