<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Rooms $model */

$this->title = 'O\'zgartirish: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Xonalar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'O\'zgartirish';
?>
<div class="rooms-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'branches'=>$branches
    ]) ?>

</div>
