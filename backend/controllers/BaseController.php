<?php

namespace backend\controllers;

use common\helpers\PermissionHelper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ErrorAction;

class BaseController extends \yii\web\Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (PermissionHelper::isAllow()) {
            return parent::beforeAction($action);
        }
        throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
                'layout' => 'auth'
            ],
        ];
    }
}