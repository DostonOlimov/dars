<?php

namespace common\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Branches;
use yii\db\ActiveQuery;

/**
 * BranchesSearch represents the model behind the search form of `common\models\Branches`.
 */
class BranchesSearch extends Branches
{
    public $state_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city_id','state_id'], 'integer'],
            [['name', 'address', 'foundated_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Branches::find();
        $query = $query->joinWith('city')->joinWith(['city' => function(ActiveQuery $query) {
            return $query->joinWith('state');
        }]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'branches.id' => $this->id,
            'branches.city_id' => $this->city_id,
            'tbl_cities.state_id'=>$this->state_id,
            'branches.foundated_at' => $this->foundated_at,
            'branches.created_at' => $this->created_at,
            'branches.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'branches.name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
