<?php

/* @var $this \yii\web\View */
/* @var $model \api\models\forms\NurseDataForm */

/* @var $nurse \common\models\Nurse */


use kartik\date\DatePicker;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Mutaxassis ma\'lumotlarini almashtirish';
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $nurse->getFullName(), 'url' => ['view', 'id' => $nurse->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';
?>


<div class="card">
    <div class="card-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'born')->widget(DatePicker::class, [
                    'options' => ['placeholder' => 'Enter birth date ...'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'series')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '[A][A]',
                    'options' => [
                        'minlength' => 2,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'number')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '[9]{7}',
                    'options' => [
                        'minlength' => 2,
                    ]
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'gender')->dropdownList(\common\models\NurseData::getGenderList()) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'branch_id')->widget(\kartik\select2\Select2::class, [
                    'data' => \common\models\Branch::getList(true),
                    'options' => [
                        'placeholder' => 'Select a state ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>

        </div>

        <div class="row">
            <div class="col-md-3">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'speciality_id')->widget(\kartik\select2\Select2::class, [
                    'data' => \common\models\Speciality::getList(true),
                    'options' => [
                        'placeholder' => 'Select a state ...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'freelancer')->dropdownList([
                    '0' => "Yo'q",
                    '1' => "Ha",
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'photo')->fileInput([
                    'accept' => 'image/*',
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col d-flex justify-content-end">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
</div>
