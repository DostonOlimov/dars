<?php

use yii\helpers\Html;
use yii\bootstrap5\ActiveForm;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var common\models\Branches $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="card">
    <div class="card-body">

    <?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-6 col-lg-6 ">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-6 col-lg-6 ">
        <?= $form->field($model, 'foundated_at')->widget(DatePicker::className(),['pluginOptions' => [
        'autoclose' => true,
        'format' => 'dd.mm.yyyy'
    ]]) ?>
    </div>
    <div class="col-md-6 col-lg-4 ">
        <?= $form->field($model, "state")->dropDownList(ArrayHelper::map($states, 'id', 'name'),['prompt'=>'- - -']) ?>
    </div>
    <div class="col-md-6 col-lg-4 ">
        <?= $form->field($model, "city_id")->widget(DepDrop::classname(), [
            'pluginOptions'=>[
            'depends'=>[Html::getInputId($model, "state")], // the id for cat attribute
            'placeholder'=>'- - -',
            'url'=>Url::to(['/cities/city'])
                ]
        ]);?>
    </div>
    <div class="col-md-6 col-lg-4 ">
        <?= $form->field($model, 'address')->textarea(['rows' => 4]) ?>
    </div>
    <div class="col d-flex justify-content-end">
        <?= Html::submitButton('Saqlash', ['class' => 'btn
                btn-success']) ?>
    </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
</div>
