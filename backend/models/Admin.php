<?php

namespace backend\models;

use common\models\User;

/**
 *
 * @property-read string $photoUrl
 */
class Admin extends User
{
    public static function find()
    {
        return parent::find()
            ->andWhere([
                'type' => self::TYPE_ADMIN
            ]);
    }
}