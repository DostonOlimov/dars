<?php

namespace common\models\defaults;

use gofuroov\multilingual\db\MultilingualQuery;
use yii\db\ActiveQuery;

class DefaultQuery extends MultilingualQuery
{
    public function active(): ActiveQuery
    {
        return $this->andWhere(['status' => DefaultActiveRecord::STATUS_ACTIVE]);
    }

    public function deleted(): ActiveQuery
    {
        return $this->andWhere(['status' => DefaultActiveRecord::STATUS_DELETED]);
    }

    public function inactive(): ActiveQuery
    {
        return $this->andWhere(['status' => DefaultActiveRecord::STATUS_INACTIVE]);
    }
}