<?php

namespace backend\models\forms;

use console\socket\ClientSocket;
use console\socket\manager\Connector;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class ChatUploadFiles extends \yii\base\Model
{
    public int $chat_id;
    public int $admin_id;

    /**
     * @var UploadedFile[]
     */
    public $files;
    public array $names = [];
    private string $workingDir = "@uploads/messages/";

    public function rules()
    {
        return [
            ['files', 'file', 'skipOnEmpty' => false, 'maxFiles' => 5]
        ];
    }

    public function upload(): bool
    {
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $name = \Yii::$app->security->generateRandomString(16) . "." . $file->extension;
                if (FileHelper::createDirectory(\Yii::getAlias($this->workingDir)) && $file->saveAs($this->workingDir . $name)) {
                    $this->names[] = "/uploads/messages/" . $name;
                }
            }
            return true;
        }

        return false;
    }

    public function send(): void
    {
        ClientSocket::send(Connector::ADMIN_SEND_FILES, [
            'chat_id' => $this->chat_id,
            'files' => $this->names,
            'admin_id' => $this->admin_id
        ]);
    }
}