<?php

namespace common\models\searchs;

use common\models\TeacherData;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TeacherDataSearch represents the model behind the search form of `common\models\TeacherData`.
 */
class TeacherDataSearch extends TeacherData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'teacher_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['born', 'photo_url',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
// bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeacherData::find();

// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
// uncomment the following line if you do not want to return any records when validation fails
// $query->where('0=1');
            return $dataProvider;
        }

// grid filtering conditions
        $query->andFilterWhere([
            'teacher_data.id' => $this->id,
            'teacher_data.Teacher_id' => $this->Teacher_id,
            'teacher_data.born' => $this->born,
            'teacher_data.status' => $this->status,
            'teacher_data.created_at' => $this->created_at,
            'teacher_data.updated_at' => $this->updated_at,
            'teacher_data.created_by' => $this->created_by,
            'teacher_data.updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'teacher_data.photo_url', $this->photo_url]);

        return $dataProvider;
    }
}
