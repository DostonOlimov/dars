<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var common\models\Rooms $model */

$this->title = 'Qo\'shish';
$this->params['breadcrumbs'][] = ['label' => 'Xonalar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rooms-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'branches'=>$branches
    ]) ?>

</div>
