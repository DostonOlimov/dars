<?php

use common\models\Teacher;
use common\models\TeacherData;
use common\models\Order;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var $this yii\web\View
 * @var $model common\models\Teacher
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $balanceProvider yii\data\ActiveDataProvider
 * @var $orderControlProvider yii\data\ActiveDataProvider
 */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'O\'qituvchilar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->isActive() ? $this->params['delete'] = Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni o\'chirmoqchimisiz?',
        'method' => 'post',
    ],
]) :
    $this->params['other'] = Html::a('<i class="fa fa-check-circle"></i>', ['activate', 'id' => $model->id], [
        'class' => 'btn btn-success',
        'title' => 'Aktivlashtirish',
        'data' => [
            'confirm' => 'Siz rostdan ham ushbu elementni aktivlashtirmoqchimisiz?',
            'method' => 'post',
        ],
    ]);
$this->params['other'] = ($this->params['other'] ?? ' ') . Html::a('Hisob tarixi', ['balance-history', 'id' => $model->id], [
        'class' => 'btn btn-info',
    ])
?>


<div class="row">
    <div class="col">
        <h3 class="text-center">«<?= $model->getFullname() ?>» </h3>
        <p class="text-center" style="font-size: xxx-large">
            <?php // Yii::$app->formatter->asCurrency($model->getTotalBalance()) ?>
        </p>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Asosiy ma'lumotlar
                </h3>
                <div class="card-tools">
                    <?= Html::a('<i class="fa fa-pencil mr-2"></i> Tahrirlash', ['update', 'id' => $model->id], ['class' => 'btn btn-tool']) ?>
                    <?= Html::a('<i class="fa fa-lock mr-2"></i> Parolni almashtirish', ['update-password', 'id' => $model->id], ['class' => 'btn btn-tool']) ?>
                </div>
            </div>
            <div class="card-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'first_name',
                        'last_name',
                        'father_name',
                        'phone',
                        'photo',
                        [
                            'attribute' => 'password',
                            'format' => 'raw',
                            'value' => static function (Teacher $teacher) {
                                return $teacher->validatePassword('12312389') ? '<span class="text-success">' . 'Odatiy parol: 12312389' . '</span>' : '<span class="text-danger">' . 'Parol o\'zgartirilgan' . '</span>';
                            }
                        ],
                        'auth_key',
                        [
                            'attribute' => 'type',
                            'value' => static function (Teacher $teacherData) {
                                return $teacherData->getTypeName();
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'value' => static function (Teacher $teacher) {
                                return $teacher->getStatusName();
                            }
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Rasmi
                </h3>
            </div>
            <div class="card-body">
                <?= Html::img($model->getPhotoUrl(), [
                    'class' => 'img-fluid img-circle img-responsive full-width',
                    'alt' => 'Teacher Data Photo',
                    'style' => 'width: 400px;height: 400px;'
                ]) ?>
            </div>
        </div>
    </div>
    <?php if ($model->teacherData): ?>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Qo'shimcha ma'lumotlari
                    </h3>
                    <div class="card-tools">
                        <?= Html::a('<i class="fa fa-pencil mr-2"></i> Tahrirlash', ['update-data', 'id' => $model->id], ['class' => 'btn btn-tool']) ?>
                    </div>
                </div>
                <div class="card-body">
                    <?= DetailView::widget([
                        'model' => $model->teacherData,
                        'attributes' => [
                            'id',
                            'born:date',
                            [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => static function (TeacherData $teacher) {
                                    return $teacher->getStatusName();
                                }
                            ],
                            'address',
                            [
                                'attribute' => 'speciality_id',
                                'format' => 'raw',
                                'value' => static function (TeacherData $teacher) {
                                    return Html::a($teacher->speciality->name, ['/speciality/view', 'id' => $teacher->speciality_id], [
                                        'data-pjax' => 0,
                                    ]);
                                }
                            ],
                            [
                                'attribute' => 'gender',
                                'value' => static function (TeacherData $teacher) {
                                    return $teacher->getGenderName();
                                }
                            ],
                            [
                                'attribute' => 'branch_id',
                                'format' => 'raw',
                                'value' => static function (TeacherData $teacher) {
                                    return Html::a($teacher->branch->name, ['/branch/view', 'id' => $teacher->branch_id], [
                                        'data-pjax' => 0,
                                    ]);
                                }
                            ],
                            [
                                    'attribute' => 'pics',
                                'format' => 'raw',
                                'value' => function (TeacherData $model) {
                                    $html = '';
                                    $pics = json_decode($model->pics, true);

                                    if (is_array($pics)) {
                                        foreach ($pics ?? [] as $img) {
                                            $html .= "<img src=\"/uploads/teacher-data/{$img}\" width='80px' class='p-1'>";
                                        }
                                    }

                                    return $html;
                                }
                            ],

                            'created_at:datetime',
                            'updated_at:datetime',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <!--<div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Tasdiqlash rasmi
                    </h3>
                </div>
                <div class="card-body">
                    <?/*= Html::img($model->TeacherData->getUrl(), [
                        'class' => 'img-fluid',
                        'alt' => 'Teacher Data Photo',
                    ]) */?>
                </div>
            </div>-->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Tasdiqlash rasmi
                    </h3>
                </div>
                <div class="card-body">
                    <?php
                       $pics = json_decode($model->teacherData->pics);
                       if($pics)
                        foreach ($pics as $p) {
                            echo Html::img("/uploads/teacher-data/" . $p, [
                                'class' => 'img-fluid p-2',
                                'alt' => 'Teacher Data Photo',
                            ]);
                        }
                    ?>
                </div>
            </div>
        </div>

    <?php endif; ?>
</div>




