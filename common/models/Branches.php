<?php

namespace common\models;

use Yii;
use common\models\defaults\DefaultActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "branches".
 *
 * @property int $id
 * @property string $name
 * @property int $city_id
 * @property string|null $address
 * @property string|null $foundated_at
 * @property string $created_at
 * @property string|null $updated_at
 *
 * @property Rooms[] $rooms
 */
class Branches extends DefaultActiveRecord
{
    public $state;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branches';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'city_id','address'], 'required'],
            [['city_id'], 'integer'],
            [['address'], 'string'],
            [['foundated_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 250],
        ];
    }
    // public function beforeSave($insert)
    // {
    //     $this->foundated_at = strtotime($this->foundated_at);
    
    //     return true;
    // }
    // public function afterFind()
    // {
    //     $this->foundated_at = $this->foundated_at ? Yii::$app->formatter->asDate($this->foundated_at, 'dd.MM.yyyy') : $this->foundated_at;
      
    // }

    public function getCity()
    {
        return $this->hasOne(Cities::class,['id'=>'city_id']);
    }
    /**
     * Gets query for [[Rooms]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRooms()
    {
        return $this->hasMany(Rooms::class, ['branch_id' => 'id']);
    }
}
