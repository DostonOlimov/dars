<?php

use common\models\Employee;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="card">
    <div class="card-body">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'father_name')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::class, [
                    'mask' => '\+\9\9\8 99 999 99 99',
                    'options' => [
                        'minlength' => 17,
                    ]
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type')->dropdownList([
                    Employee::TYPE_MANAGER => 'Boshqaruvchi',
                    Employee::TYPE_ADMINSTRATOR => 'Adminstrator',
                ]) ?>
            </div>
        </div>

        <?= $form->field($model, 'branches')->widget(\kartik\select2\Select2::class, [
            'data' => \common\models\Branches::getList(),
            'options' => [
                'placeholder' => 'Select a state ...',
                'multiple' => true,
                'id' => 'languages'
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

        <div class="row">
            <div class="col d-flex justify-content-end">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>