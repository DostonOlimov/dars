<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_states".
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string|null $series
 * @property int $country_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int $list_id
 * @property int $soato
 *
 * @property TblCities[] $tblCities
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_states';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code', 'list_id'], 'required'],
            [['country_id', 'list_id', 'soato'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 30],
            [['code'], 'string', 'max' => 11],
            [['series'], 'string', 'max' => 12],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'series' => 'Series',
            'country_id' => 'Country ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'list_id' => 'List ID',
            'soato' => 'Soato',
        ];
    }

    /**
     * Gets query for [[TblCities]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTblCities()
    {
        return $this->hasMany(TblCities::class, ['state_id' => 'id']);
    }
}
