<?php

namespace backend\models\forms;

use common\models\Balance;
use common\models\Nurse;

class PaynetForm extends \yii\base\Model
{
    public ?int $nurse_id = null;
    public int $money = 0;
    public ?string $info = null;

    private ?Nurse $nurse;

    public function rules()
    {
        return [
            [['nurse_id', 'money'], 'required'],
            [['nurse_id', 'money'], 'integer'],
            [['info'], 'string'],
            ['money', 'compare', 'compareValue' => 0, 'operator' => '!='],
            [['nurse_id'], 'exist', 'targetClass' => Nurse::class, 'targetAttribute' => ['nurse_id' => 'id']],
        ];
    }

    /**
     * @throws \yii\web\ServerErrorHttpException
     */
    public function pay(): bool
    {
        if (!$this->validate()) {
            return false;
        }
        $this->nurse = Nurse::findOne($this->nurse_id);

        if ($this->nurse->addBalance($this->money, $this->money > 0 ? Balance::REASON_FILL_BALANCE : Balance::REASON_REDUCE_BALANCE, null, $this->info)) {
            return true;
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'nurse_id' => "Mutaxassis",
            'money' => "Pul miqdori",
            'info' => 'izoh'
        ];
    }

    public function attributeHints()
    {
        return [
            'money' => "Agar pul muqdori manfiy kiritilsa, u holda pul yechib olinadi va chiqim sifatida avtomatik belgilanadi"
        ];
    }
}