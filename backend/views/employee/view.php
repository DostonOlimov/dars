<?php

use common\models\Employee;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Employee */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Ishchilar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['update'] = Html::a('<i class="fa fa-pencil"></i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);

$this->params['delete'] = $model->status === Employee::STATUS_ACTIVE ? Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-warning',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni o\'chirmoqchimisiz?',
        'method' => 'post',
    ],
]) : '';

$this->params['back'] = $model->status === Employee::STATUS_DELETED ? Html::a('<i class="fa fa-trash"></i>', ['force-delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni o\'chirmoqchimisiz?',
        'method' => 'post',
    ],
]) : '';
$this->params['other'] = !$model->isActive() ? Html::a('<i class="fa fa-recycle"></i>', ['activate', 'id' => $model->id], [
    'class' => 'btn btn-success',
    'data' => [
        'confirm' => 'Siz rostdan ham ushbu elementni faollashtirmoqchimisiz?',
        'method' => 'post',
    ],
]) : '';

?>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Asosiy ma'lumotlar
                </h3>
            </div>
            <div class="card-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'first_name',
                        'last_name',
                        'father_name',
                        'phone',
                        'username',
                        [
                            'attribute' => 'password',
                            'format' => 'raw',
                            'value' => static function (Employee $employee) {
                                return $employee->validatePassword('12345678') ? '<span class="text-success">' . 'Odatiy parol: 12345678' . '</span>' : '<span class="text-danger">' . 'Parol o\'zgartirilgan' . '</span>';
                            }
                        ],
                        [
                            'attribute' => 'type',
                            'value' => static function (Employee $nurseData) {
                                return $nurseData->getTypeName();
                            }
                        ],
                        [
                            'attribute' => 'branches',
                            'format' => 'raw',
                            'value' => static function (Employee $nurseData) {
                                $items = '';
                                foreach ($nurseData->employeeBranches as $branch) {
                                    $items .= "<li>{$branch->branch->name}</li>";
                                }
                                return "<ul>{$items}</ul>";
                            }
                        ],
                        [
                            'attribute' => 'status',
                            'value' => static function (Employee $nurse) {
                                return $nurse->getStatusName();
                            }
                        ],
                        'created_at:datetime',
                        'updated_at:datetime',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">
                    Rasmi
                </h3>
            </div>
            <div class="card-body">
                <?= Html::img($model->getPhotoUrl(), [
                    'class' => 'img-fluid img-circle',
                    'alt' => 'Nurse Data Photo',
                ]) ?>
            </div>
        </div>
    </div>
</div>