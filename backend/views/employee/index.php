<?php

use common\models\Employee;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\searchs\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ishchilar';
$this->params['breadcrumbs'][] = $this->title;
$this->params['create'] = Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn btn-success'])
?>

<?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Faollar
                    </div>
                    <div class="card-tools">
                        <?= \yii\bootstrap4\Html::a('Arxiv', ['employee/archive'], [
                            'data-pjax' => 0
                        ]) ?>
                    </div>
                </div>
                <div class="card-body">
                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'first_name',
                            'last_name',
                            'phone',
                            [
                                'attribute' => 'type',
                                'value' => static function (Employee $employee) {
                                    return $employee->getTypeName();
                                },
                                'filter' => [
                                    Employee::TYPE_MANAGER => 'Boshqaruvchi',
                                    Employee::TYPE_ADMINSTRATOR => 'Adminstrator',
                                ],
                            ],
                            [
                                'class' => \common\columns\StatusColumn::class,
                                'filterList' => Employee::getStatusList(),
                            ],
                            [
                                'class' => ActionColumn::className(),
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>
<?php Pjax::end(); ?>