<?php

/* @var $this \yii\web\View */
/* @var $model \backend\models\forms\TeacherPasswordForm */

/* @var $teacher \common\models\Teacher */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = 'Mutaxassis parolini almashtirish';
$this->params['breadcrumbs'][] = ['label' => 'Mutaxassislar', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $teacher->getFullName(), 'url' => ['view', 'id' => $teacher->id]];
$this->params['breadcrumbs'][] = 'Tahrirlash';
?>


<div class="card">
    <div class="card-body">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'password_repeat')->passwordInput() ?>

        <div class="row">
            <div class="col d-flex justify-content-end">
                <?= Html::submitButton('Saqlash', ['class' => 'btn btn-success']) ?>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
</div>

